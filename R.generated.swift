//
// This is a generated file, do not edit!
// Generated by R.swift, see https://github.com/mac-cain13/R.swift
//

import Foundation
import Rswift
import UIKit

/// This `R` struct is generated and contains references to static resources.
struct R: Rswift.Validatable {
  fileprivate static let applicationLocale = hostingBundle.preferredLocalizations.first.flatMap { Locale(identifier: $0) } ?? Locale.current
  fileprivate static let hostingBundle = Bundle(for: R.Class.self)

  /// Find first language and bundle for which the table exists
  fileprivate static func localeBundle(tableName: String, preferredLanguages: [String]) -> (Foundation.Locale, Foundation.Bundle)? {
    // Filter preferredLanguages to localizations, use first locale
    var languages = preferredLanguages
      .map { Locale(identifier: $0) }
      .prefix(1)
      .flatMap { locale -> [String] in
        if hostingBundle.localizations.contains(locale.identifier) {
          if let language = locale.languageCode, hostingBundle.localizations.contains(language) {
            return [locale.identifier, language]
          } else {
            return [locale.identifier]
          }
        } else if let language = locale.languageCode, hostingBundle.localizations.contains(language) {
          return [language]
        } else {
          return []
        }
      }

    // If there's no languages, use development language as backstop
    if languages.isEmpty {
      if let developmentLocalization = hostingBundle.developmentLocalization {
        languages = [developmentLocalization]
      }
    } else {
      // Insert Base as second item (between locale identifier and languageCode)
      languages.insert("Base", at: 1)

      // Add development language as backstop
      if let developmentLocalization = hostingBundle.developmentLocalization {
        languages.append(developmentLocalization)
      }
    }

    // Find first language for which table exists
    // Note: key might not exist in chosen language (in that case, key will be shown)
    for language in languages {
      if let lproj = hostingBundle.url(forResource: language, withExtension: "lproj"),
         let lbundle = Bundle(url: lproj)
      {
        let strings = lbundle.url(forResource: tableName, withExtension: "strings")
        let stringsdict = lbundle.url(forResource: tableName, withExtension: "stringsdict")

        if strings != nil || stringsdict != nil {
          return (Locale(identifier: language), lbundle)
        }
      }
    }

    // If table is available in main bundle, don't look for localized resources
    let strings = hostingBundle.url(forResource: tableName, withExtension: "strings", subdirectory: nil, localization: nil)
    let stringsdict = hostingBundle.url(forResource: tableName, withExtension: "stringsdict", subdirectory: nil, localization: nil)

    if strings != nil || stringsdict != nil {
      return (applicationLocale, hostingBundle)
    }

    // If table is not found for requested languages, key will be shown
    return nil
  }

  /// Load string from Info.plist file
  fileprivate static func infoPlistString(path: [String], key: String) -> String? {
    var dict = hostingBundle.infoDictionary
    for step in path {
      guard let obj = dict?[step] as? [String: Any] else { return nil }
      dict = obj
    }
    return dict?[key] as? String
  }

  static func validate() throws {
    try font.validate()
    try intern.validate()
  }

  #if os(iOS) || os(tvOS)
  /// This `R.storyboard` struct is generated, and contains static references to 5 storyboards.
  struct storyboard {
    /// Storyboard `Authentication`.
    static let authentication = _R.storyboard.authentication()
    /// Storyboard `Feeds`.
    static let feeds = _R.storyboard.feeds()
    /// Storyboard `Home`.
    static let home = _R.storyboard.home()
    /// Storyboard `LaunchScreen`.
    static let launchScreen = _R.storyboard.launchScreen()
    /// Storyboard `Profile`.
    static let profile = _R.storyboard.profile()

    #if os(iOS) || os(tvOS)
    /// `UIStoryboard(name: "Authentication", bundle: ...)`
    static func authentication(_: Void = ()) -> UIKit.UIStoryboard {
      return UIKit.UIStoryboard(resource: R.storyboard.authentication)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIStoryboard(name: "Feeds", bundle: ...)`
    static func feeds(_: Void = ()) -> UIKit.UIStoryboard {
      return UIKit.UIStoryboard(resource: R.storyboard.feeds)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIStoryboard(name: "Home", bundle: ...)`
    static func home(_: Void = ()) -> UIKit.UIStoryboard {
      return UIKit.UIStoryboard(resource: R.storyboard.home)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIStoryboard(name: "LaunchScreen", bundle: ...)`
    static func launchScreen(_: Void = ()) -> UIKit.UIStoryboard {
      return UIKit.UIStoryboard(resource: R.storyboard.launchScreen)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIStoryboard(name: "Profile", bundle: ...)`
    static func profile(_: Void = ()) -> UIKit.UIStoryboard {
      return UIKit.UIStoryboard(resource: R.storyboard.profile)
    }
    #endif

    fileprivate init() {}
  }
  #endif

  /// This `R.color` struct is generated, and contains static references to 2 colors.
  struct color {
    /// Color `AccentColor`.
    static let accentColor = Rswift.ColorResource(bundle: R.hostingBundle, name: "AccentColor")
    /// Color `MainColor`.
    static let mainColor = Rswift.ColorResource(bundle: R.hostingBundle, name: "MainColor")

    #if os(iOS) || os(tvOS)
    /// `UIColor(named: "AccentColor", bundle: ..., traitCollection: ...)`
    @available(tvOS 11.0, *)
    @available(iOS 11.0, *)
    static func accentColor(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIColor? {
      return UIKit.UIColor(resource: R.color.accentColor, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIColor(named: "MainColor", bundle: ..., traitCollection: ...)`
    @available(tvOS 11.0, *)
    @available(iOS 11.0, *)
    static func mainColor(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIColor? {
      return UIKit.UIColor(resource: R.color.mainColor, compatibleWith: traitCollection)
    }
    #endif

    #if os(watchOS)
    /// `UIColor(named: "AccentColor", bundle: ..., traitCollection: ...)`
    @available(watchOSApplicationExtension 4.0, *)
    static func accentColor(_: Void = ()) -> UIKit.UIColor? {
      return UIKit.UIColor(named: R.color.accentColor.name)
    }
    #endif

    #if os(watchOS)
    /// `UIColor(named: "MainColor", bundle: ..., traitCollection: ...)`
    @available(watchOSApplicationExtension 4.0, *)
    static func mainColor(_: Void = ()) -> UIKit.UIColor? {
      return UIKit.UIColor(named: R.color.mainColor.name)
    }
    #endif

    fileprivate init() {}
  }

  /// This `R.file` struct is generated, and contains static references to 6 files.
  struct file {
    /// Resource file `README.md`.
    static let readmeMd = Rswift.FileResource(bundle: R.hostingBundle, name: "README", pathExtension: "md")
    /// Resource file `RobotoSlab-Bold.ttf`.
    static let robotoSlabBoldTtf = Rswift.FileResource(bundle: R.hostingBundle, name: "RobotoSlab-Bold", pathExtension: "ttf")
    /// Resource file `RobotoSlab-ExtraBold.ttf`.
    static let robotoSlabExtraBoldTtf = Rswift.FileResource(bundle: R.hostingBundle, name: "RobotoSlab-ExtraBold", pathExtension: "ttf")
    /// Resource file `RobotoSlab-Light.ttf`.
    static let robotoSlabLightTtf = Rswift.FileResource(bundle: R.hostingBundle, name: "RobotoSlab-Light", pathExtension: "ttf")
    /// Resource file `RobotoSlab-Medium.ttf`.
    static let robotoSlabMediumTtf = Rswift.FileResource(bundle: R.hostingBundle, name: "RobotoSlab-Medium", pathExtension: "ttf")
    /// Resource file `RobotoSlab-Regular.ttf`.
    static let robotoSlabRegularTtf = Rswift.FileResource(bundle: R.hostingBundle, name: "RobotoSlab-Regular", pathExtension: "ttf")

    /// `bundle.url(forResource: "README", withExtension: "md")`
    static func readmeMd(_: Void = ()) -> Foundation.URL? {
      let fileResource = R.file.readmeMd
      return fileResource.bundle.url(forResource: fileResource)
    }

    /// `bundle.url(forResource: "RobotoSlab-Bold", withExtension: "ttf")`
    static func robotoSlabBoldTtf(_: Void = ()) -> Foundation.URL? {
      let fileResource = R.file.robotoSlabBoldTtf
      return fileResource.bundle.url(forResource: fileResource)
    }

    /// `bundle.url(forResource: "RobotoSlab-ExtraBold", withExtension: "ttf")`
    static func robotoSlabExtraBoldTtf(_: Void = ()) -> Foundation.URL? {
      let fileResource = R.file.robotoSlabExtraBoldTtf
      return fileResource.bundle.url(forResource: fileResource)
    }

    /// `bundle.url(forResource: "RobotoSlab-Light", withExtension: "ttf")`
    static func robotoSlabLightTtf(_: Void = ()) -> Foundation.URL? {
      let fileResource = R.file.robotoSlabLightTtf
      return fileResource.bundle.url(forResource: fileResource)
    }

    /// `bundle.url(forResource: "RobotoSlab-Medium", withExtension: "ttf")`
    static func robotoSlabMediumTtf(_: Void = ()) -> Foundation.URL? {
      let fileResource = R.file.robotoSlabMediumTtf
      return fileResource.bundle.url(forResource: fileResource)
    }

    /// `bundle.url(forResource: "RobotoSlab-Regular", withExtension: "ttf")`
    static func robotoSlabRegularTtf(_: Void = ()) -> Foundation.URL? {
      let fileResource = R.file.robotoSlabRegularTtf
      return fileResource.bundle.url(forResource: fileResource)
    }

    fileprivate init() {}
  }

  /// This `R.font` struct is generated, and contains static references to 5 fonts.
  struct font: Rswift.Validatable {
    /// Font `RobotoSlab-Bold`.
    static let robotoSlabBold = Rswift.FontResource(fontName: "RobotoSlab-Bold")
    /// Font `RobotoSlab-ExtraBold`.
    static let robotoSlabExtraBold = Rswift.FontResource(fontName: "RobotoSlab-ExtraBold")
    /// Font `RobotoSlab-Light`.
    static let robotoSlabLight = Rswift.FontResource(fontName: "RobotoSlab-Light")
    /// Font `RobotoSlab-Medium`.
    static let robotoSlabMedium = Rswift.FontResource(fontName: "RobotoSlab-Medium")
    /// Font `RobotoSlab-Regular`.
    static let robotoSlabRegular = Rswift.FontResource(fontName: "RobotoSlab-Regular")

    /// `UIFont(name: "RobotoSlab-Bold", size: ...)`
    static func robotoSlabBold(size: CGFloat) -> UIKit.UIFont? {
      return UIKit.UIFont(resource: robotoSlabBold, size: size)
    }

    /// `UIFont(name: "RobotoSlab-ExtraBold", size: ...)`
    static func robotoSlabExtraBold(size: CGFloat) -> UIKit.UIFont? {
      return UIKit.UIFont(resource: robotoSlabExtraBold, size: size)
    }

    /// `UIFont(name: "RobotoSlab-Light", size: ...)`
    static func robotoSlabLight(size: CGFloat) -> UIKit.UIFont? {
      return UIKit.UIFont(resource: robotoSlabLight, size: size)
    }

    /// `UIFont(name: "RobotoSlab-Medium", size: ...)`
    static func robotoSlabMedium(size: CGFloat) -> UIKit.UIFont? {
      return UIKit.UIFont(resource: robotoSlabMedium, size: size)
    }

    /// `UIFont(name: "RobotoSlab-Regular", size: ...)`
    static func robotoSlabRegular(size: CGFloat) -> UIKit.UIFont? {
      return UIKit.UIFont(resource: robotoSlabRegular, size: size)
    }

    static func validate() throws {
      if R.font.robotoSlabBold(size: 42) == nil { throw Rswift.ValidationError(description:"[R.swift] Font 'RobotoSlab-Bold' could not be loaded, is 'RobotoSlab-Bold.ttf' added to the UIAppFonts array in this targets Info.plist?") }
      if R.font.robotoSlabExtraBold(size: 42) == nil { throw Rswift.ValidationError(description:"[R.swift] Font 'RobotoSlab-ExtraBold' could not be loaded, is 'RobotoSlab-ExtraBold.ttf' added to the UIAppFonts array in this targets Info.plist?") }
      if R.font.robotoSlabLight(size: 42) == nil { throw Rswift.ValidationError(description:"[R.swift] Font 'RobotoSlab-Light' could not be loaded, is 'RobotoSlab-Light.ttf' added to the UIAppFonts array in this targets Info.plist?") }
      if R.font.robotoSlabMedium(size: 42) == nil { throw Rswift.ValidationError(description:"[R.swift] Font 'RobotoSlab-Medium' could not be loaded, is 'RobotoSlab-Medium.ttf' added to the UIAppFonts array in this targets Info.plist?") }
      if R.font.robotoSlabRegular(size: 42) == nil { throw Rswift.ValidationError(description:"[R.swift] Font 'RobotoSlab-Regular' could not be loaded, is 'RobotoSlab-Regular.ttf' added to the UIAppFonts array in this targets Info.plist?") }
    }

    fileprivate init() {}
  }

  /// This `R.image` struct is generated, and contains static references to 21 images.
  struct image {
    /// Image `Editor'sChoice`.
    static let editorSChoice = Rswift.ImageResource(bundle: R.hostingBundle, name: "Editor'sChoice")
    /// Image `LaunchScreenImage`.
    static let launchScreenImage = Rswift.ImageResource(bundle: R.hostingBundle, name: "LaunchScreenImage")
    /// Image `SourceImage`.
    static let sourceImage = Rswift.ImageResource(bundle: R.hostingBundle, name: "SourceImage")
    /// Image `Sources`.
    static let sources = Rswift.ImageResource(bundle: R.hostingBundle, name: "Sources")
    /// Image `bookmark`.
    static let bookmark = Rswift.ImageResource(bundle: R.hostingBundle, name: "bookmark")
    /// Image `facebook`.
    static let facebook = Rswift.ImageResource(bundle: R.hostingBundle, name: "facebook")
    /// Image `fill_line`.
    static let fill_line = Rswift.ImageResource(bundle: R.hostingBundle, name: "fill_line")
    /// Image `google`.
    static let google = Rswift.ImageResource(bundle: R.hostingBundle, name: "google")
    /// Image `invite`.
    static let invite = Rswift.ImageResource(bundle: R.hostingBundle, name: "invite")
    /// Image `like`.
    static let like = Rswift.ImageResource(bundle: R.hostingBundle, name: "like")
    /// Image `line`.
    static let line = Rswift.ImageResource(bundle: R.hostingBundle, name: "line")
    /// Image `login`.
    static let login = Rswift.ImageResource(bundle: R.hostingBundle, name: "login")
    /// Image `logout`.
    static let logout = Rswift.ImageResource(bundle: R.hostingBundle, name: "logout")
    /// Image `privacy-policy`.
    static let privacyPolicy = Rswift.ImageResource(bundle: R.hostingBundle, name: "privacy-policy")
    /// Image `profile`.
    static let profile = Rswift.ImageResource(bundle: R.hostingBundle, name: "profile")
    /// Image `rating`.
    static let rating = Rswift.ImageResource(bundle: R.hostingBundle, name: "rating")
    /// Image `refresh`.
    static let refresh = Rswift.ImageResource(bundle: R.hostingBundle, name: "refresh")
    /// Image `search`.
    static let search = Rswift.ImageResource(bundle: R.hostingBundle, name: "search")
    /// Image `share`.
    static let share = Rswift.ImageResource(bundle: R.hostingBundle, name: "share")
    /// Image `terms-and-conditions`.
    static let termsAndConditions = Rswift.ImageResource(bundle: R.hostingBundle, name: "terms-and-conditions")
    /// Image `top`.
    static let top = Rswift.ImageResource(bundle: R.hostingBundle, name: "top")

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "Editor'sChoice", bundle: ..., traitCollection: ...)`
    static func editorSChoice(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.editorSChoice, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "LaunchScreenImage", bundle: ..., traitCollection: ...)`
    static func launchScreenImage(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.launchScreenImage, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "SourceImage", bundle: ..., traitCollection: ...)`
    static func sourceImage(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.sourceImage, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "Sources", bundle: ..., traitCollection: ...)`
    static func sources(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.sources, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "bookmark", bundle: ..., traitCollection: ...)`
    static func bookmark(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.bookmark, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "facebook", bundle: ..., traitCollection: ...)`
    static func facebook(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.facebook, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "fill_line", bundle: ..., traitCollection: ...)`
    static func fill_line(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.fill_line, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "google", bundle: ..., traitCollection: ...)`
    static func google(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.google, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "invite", bundle: ..., traitCollection: ...)`
    static func invite(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.invite, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "like", bundle: ..., traitCollection: ...)`
    static func like(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.like, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "line", bundle: ..., traitCollection: ...)`
    static func line(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.line, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "login", bundle: ..., traitCollection: ...)`
    static func login(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.login, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "logout", bundle: ..., traitCollection: ...)`
    static func logout(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.logout, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "privacy-policy", bundle: ..., traitCollection: ...)`
    static func privacyPolicy(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.privacyPolicy, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "profile", bundle: ..., traitCollection: ...)`
    static func profile(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.profile, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "rating", bundle: ..., traitCollection: ...)`
    static func rating(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.rating, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "refresh", bundle: ..., traitCollection: ...)`
    static func refresh(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.refresh, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "search", bundle: ..., traitCollection: ...)`
    static func search(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.search, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "share", bundle: ..., traitCollection: ...)`
    static func share(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.share, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "terms-and-conditions", bundle: ..., traitCollection: ...)`
    static func termsAndConditions(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.termsAndConditions, compatibleWith: traitCollection)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UIImage(named: "top", bundle: ..., traitCollection: ...)`
    static func top(compatibleWith traitCollection: UIKit.UITraitCollection? = nil) -> UIKit.UIImage? {
      return UIKit.UIImage(resource: R.image.top, compatibleWith: traitCollection)
    }
    #endif

    fileprivate init() {}
  }

  /// This `R.info` struct is generated, and contains static references to 1 properties.
  struct info {
    struct uiApplicationSceneManifest {
      static let _key = "UIApplicationSceneManifest"
      static let uiApplicationSupportsMultipleScenes = false

      struct uiSceneConfigurations {
        static let _key = "UISceneConfigurations"

        struct uiWindowSceneSessionRoleApplication {
          struct defaultConfiguration {
            static let _key = "Default Configuration"
            static let uiSceneConfigurationName = infoPlistString(path: ["UIApplicationSceneManifest", "UISceneConfigurations", "UIWindowSceneSessionRoleApplication", "Default Configuration"], key: "UISceneConfigurationName") ?? "Default Configuration"
            static let uiSceneDelegateClassName = infoPlistString(path: ["UIApplicationSceneManifest", "UISceneConfigurations", "UIWindowSceneSessionRoleApplication", "Default Configuration"], key: "UISceneDelegateClassName") ?? "$(PRODUCT_MODULE_NAME).SceneDelegate"
            static let uiSceneStoryboardFile = infoPlistString(path: ["UIApplicationSceneManifest", "UISceneConfigurations", "UIWindowSceneSessionRoleApplication", "Default Configuration"], key: "UISceneStoryboardFile") ?? "Home"

            fileprivate init() {}
          }

          fileprivate init() {}
        }

        fileprivate init() {}
      }

      fileprivate init() {}
    }

    fileprivate init() {}
  }

  /// This `R.nib` struct is generated, and contains static references to 3 nibs.
  struct nib {
    /// Nib `CollectionViewCellForHome`.
    static let collectionViewCellForHome = _R.nib._CollectionViewCellForHome()
    /// Nib `TableViewCellForFeed`.
    static let tableViewCellForFeed = _R.nib._TableViewCellForFeed()
    /// Nib `TableViewCellForProfle`.
    static let tableViewCellForProfle = _R.nib._TableViewCellForProfle()

    #if os(iOS) || os(tvOS)
    /// `UINib(name: "CollectionViewCellForHome", in: bundle)`
    @available(*, deprecated, message: "Use UINib(resource: R.nib.collectionViewCellForHome) instead")
    static func collectionViewCellForHome(_: Void = ()) -> UIKit.UINib {
      return UIKit.UINib(resource: R.nib.collectionViewCellForHome)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UINib(name: "TableViewCellForFeed", in: bundle)`
    @available(*, deprecated, message: "Use UINib(resource: R.nib.tableViewCellForFeed) instead")
    static func tableViewCellForFeed(_: Void = ()) -> UIKit.UINib {
      return UIKit.UINib(resource: R.nib.tableViewCellForFeed)
    }
    #endif

    #if os(iOS) || os(tvOS)
    /// `UINib(name: "TableViewCellForProfle", in: bundle)`
    @available(*, deprecated, message: "Use UINib(resource: R.nib.tableViewCellForProfle) instead")
    static func tableViewCellForProfle(_: Void = ()) -> UIKit.UINib {
      return UIKit.UINib(resource: R.nib.tableViewCellForProfle)
    }
    #endif

    static func collectionViewCellForHome(owner ownerOrNil: AnyObject?, options optionsOrNil: [UINib.OptionsKey : Any]? = nil) -> CollectionViewCellForHome? {
      return R.nib.collectionViewCellForHome.instantiate(withOwner: ownerOrNil, options: optionsOrNil)[0] as? CollectionViewCellForHome
    }

    static func tableViewCellForFeed(owner ownerOrNil: AnyObject?, options optionsOrNil: [UINib.OptionsKey : Any]? = nil) -> TableViewCellForFeed? {
      return R.nib.tableViewCellForFeed.instantiate(withOwner: ownerOrNil, options: optionsOrNil)[0] as? TableViewCellForFeed
    }

    static func tableViewCellForProfle(owner ownerOrNil: AnyObject?, options optionsOrNil: [UINib.OptionsKey : Any]? = nil) -> TableViewCellForProfle? {
      return R.nib.tableViewCellForProfle.instantiate(withOwner: ownerOrNil, options: optionsOrNil)[0] as? TableViewCellForProfle
    }

    fileprivate init() {}
  }

  /// This `R.reuseIdentifier` struct is generated, and contains static references to 3 reuse identifiers.
  struct reuseIdentifier {
    /// Reuse identifier `CollectionViewCellForHome`.
    static let collectionViewCellForHome: Rswift.ReuseIdentifier<CollectionViewCellForHome> = Rswift.ReuseIdentifier(identifier: "CollectionViewCellForHome")
    /// Reuse identifier `TableViewCellForFeed`.
    static let tableViewCellForFeed: Rswift.ReuseIdentifier<TableViewCellForFeed> = Rswift.ReuseIdentifier(identifier: "TableViewCellForFeed")
    /// Reuse identifier `TableViewCellForProfle`.
    static let tableViewCellForProfle: Rswift.ReuseIdentifier<TableViewCellForProfle> = Rswift.ReuseIdentifier(identifier: "TableViewCellForProfle")

    fileprivate init() {}
  }

  fileprivate struct intern: Rswift.Validatable {
    fileprivate static func validate() throws {
      try _R.validate()
    }

    fileprivate init() {}
  }

  fileprivate class Class {}

  fileprivate init() {}
}

struct _R: Rswift.Validatable {
  static func validate() throws {
    #if os(iOS) || os(tvOS)
    try nib.validate()
    #endif
    #if os(iOS) || os(tvOS)
    try storyboard.validate()
    #endif
  }

  #if os(iOS) || os(tvOS)
  struct nib: Rswift.Validatable {
    static func validate() throws {
      try _TableViewCellForFeed.validate()
    }

    struct _CollectionViewCellForHome: Rswift.NibResourceType, Rswift.ReuseIdentifierType {
      typealias ReusableType = CollectionViewCellForHome

      let bundle = R.hostingBundle
      let identifier = "CollectionViewCellForHome"
      let name = "CollectionViewCellForHome"

      func firstView(owner ownerOrNil: AnyObject?, options optionsOrNil: [UINib.OptionsKey : Any]? = nil) -> CollectionViewCellForHome? {
        return instantiate(withOwner: ownerOrNil, options: optionsOrNil)[0] as? CollectionViewCellForHome
      }

      fileprivate init() {}
    }

    struct _TableViewCellForFeed: Rswift.NibResourceType, Rswift.ReuseIdentifierType, Rswift.Validatable {
      typealias ReusableType = TableViewCellForFeed

      let bundle = R.hostingBundle
      let identifier = "TableViewCellForFeed"
      let name = "TableViewCellForFeed"

      func firstView(owner ownerOrNil: AnyObject?, options optionsOrNil: [UINib.OptionsKey : Any]? = nil) -> TableViewCellForFeed? {
        return instantiate(withOwner: ownerOrNil, options: optionsOrNil)[0] as? TableViewCellForFeed
      }

      static func validate() throws {
        if UIKit.UIImage(named: "Editor'sChoice", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'Editor'sChoice' is used in nib 'TableViewCellForFeed', but couldn't be loaded.") }
        if UIKit.UIImage(named: "bookmark", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'bookmark' is used in nib 'TableViewCellForFeed', but couldn't be loaded.") }
        if UIKit.UIImage(named: "like", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'like' is used in nib 'TableViewCellForFeed', but couldn't be loaded.") }
        if UIKit.UIImage(named: "share", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'share' is used in nib 'TableViewCellForFeed', but couldn't be loaded.") }
        if #available(iOS 11.0, tvOS 11.0, *) {
          if UIKit.UIColor(named: "MainColor", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Color named 'MainColor' is used in nib 'TableViewCellForFeed', but couldn't be loaded.") }
        }
      }

      fileprivate init() {}
    }

    struct _TableViewCellForProfle: Rswift.NibResourceType, Rswift.ReuseIdentifierType {
      typealias ReusableType = TableViewCellForProfle

      let bundle = R.hostingBundle
      let identifier = "TableViewCellForProfle"
      let name = "TableViewCellForProfle"

      func firstView(owner ownerOrNil: AnyObject?, options optionsOrNil: [UINib.OptionsKey : Any]? = nil) -> TableViewCellForProfle? {
        return instantiate(withOwner: ownerOrNil, options: optionsOrNil)[0] as? TableViewCellForProfle
      }

      fileprivate init() {}
    }

    fileprivate init() {}
  }
  #endif

  #if os(iOS) || os(tvOS)
  struct storyboard: Rswift.Validatable {
    static func validate() throws {
      #if os(iOS) || os(tvOS)
      try authentication.validate()
      #endif
      #if os(iOS) || os(tvOS)
      try feeds.validate()
      #endif
      #if os(iOS) || os(tvOS)
      try home.validate()
      #endif
      #if os(iOS) || os(tvOS)
      try launchScreen.validate()
      #endif
      #if os(iOS) || os(tvOS)
      try profile.validate()
      #endif
    }

    #if os(iOS) || os(tvOS)
    struct authentication: Rswift.StoryboardResourceType, Rswift.Validatable {
      let authViewController = StoryboardViewControllerResource<AuthViewController>(identifier: "AuthViewController")
      let bundle = R.hostingBundle
      let name = "Authentication"

      func authViewController(_: Void = ()) -> AuthViewController? {
        return UIKit.UIStoryboard(resource: self).instantiateViewController(withResource: authViewController)
      }

      static func validate() throws {
        if UIKit.UIImage(named: "facebook", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'facebook' is used in storyboard 'Authentication', but couldn't be loaded.") }
        if UIKit.UIImage(named: "google", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'google' is used in storyboard 'Authentication', but couldn't be loaded.") }
        if #available(iOS 11.0, tvOS 11.0, *) {
          if UIKit.UIColor(named: "MainColor", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Color named 'MainColor' is used in storyboard 'Authentication', but couldn't be loaded.") }
        }
        if _R.storyboard.authentication().authViewController() == nil { throw Rswift.ValidationError(description:"[R.swift] ViewController with identifier 'authViewController' could not be loaded from storyboard 'Authentication' as 'AuthViewController'.") }
      }

      fileprivate init() {}
    }
    #endif

    #if os(iOS) || os(tvOS)
    struct feeds: Rswift.StoryboardResourceType, Rswift.Validatable {
      let bundle = R.hostingBundle
      let feedsViewController = StoryboardViewControllerResource<FeedsViewController>(identifier: "FeedsViewController")
      let name = "Feeds"

      func feedsViewController(_: Void = ()) -> FeedsViewController? {
        return UIKit.UIStoryboard(resource: self).instantiateViewController(withResource: feedsViewController)
      }

      static func validate() throws {
        if UIKit.UIImage(named: "Sources", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'Sources' is used in storyboard 'Feeds', but couldn't be loaded.") }
        if UIKit.UIImage(named: "profile", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'profile' is used in storyboard 'Feeds', but couldn't be loaded.") }
        if UIKit.UIImage(named: "refresh", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'refresh' is used in storyboard 'Feeds', but couldn't be loaded.") }
        if #available(iOS 11.0, tvOS 11.0, *) {
        }
        if _R.storyboard.feeds().feedsViewController() == nil { throw Rswift.ValidationError(description:"[R.swift] ViewController with identifier 'feedsViewController' could not be loaded from storyboard 'Feeds' as 'FeedsViewController'.") }
      }

      fileprivate init() {}
    }
    #endif

    #if os(iOS) || os(tvOS)
    struct home: Rswift.StoryboardResourceWithInitialControllerType, Rswift.Validatable {
      typealias InitialController = UIKit.UINavigationController

      let bundle = R.hostingBundle
      let homeViewController = StoryboardViewControllerResource<HomeViewController>(identifier: "HomeViewController")
      let name = "Home"

      func homeViewController(_: Void = ()) -> HomeViewController? {
        return UIKit.UIStoryboard(resource: self).instantiateViewController(withResource: homeViewController)
      }

      static func validate() throws {
        if UIKit.UIImage(named: "Editor'sChoice", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'Editor'sChoice' is used in storyboard 'Home', but couldn't be loaded.") }
        if UIKit.UIImage(named: "bookmark", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'bookmark' is used in storyboard 'Home', but couldn't be loaded.") }
        if UIKit.UIImage(named: "search", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'search' is used in storyboard 'Home', but couldn't be loaded.") }
        if #available(iOS 11.0, tvOS 11.0, *) {
          if UIKit.UIColor(named: "MainColor", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Color named 'MainColor' is used in storyboard 'Home', but couldn't be loaded.") }
        }
        if _R.storyboard.home().homeViewController() == nil { throw Rswift.ValidationError(description:"[R.swift] ViewController with identifier 'homeViewController' could not be loaded from storyboard 'Home' as 'HomeViewController'.") }
      }

      fileprivate init() {}
    }
    #endif

    #if os(iOS) || os(tvOS)
    struct launchScreen: Rswift.StoryboardResourceWithInitialControllerType, Rswift.Validatable {
      typealias InitialController = UIKit.UIViewController

      let bundle = R.hostingBundle
      let name = "LaunchScreen"

      static func validate() throws {
        if UIKit.UIImage(named: "LaunchScreenImage", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'LaunchScreenImage' is used in storyboard 'LaunchScreen', but couldn't be loaded.") }
        if #available(iOS 11.0, tvOS 11.0, *) {
        }
      }

      fileprivate init() {}
    }
    #endif

    #if os(iOS) || os(tvOS)
    struct profile: Rswift.StoryboardResourceType, Rswift.Validatable {
      let bundle = R.hostingBundle
      let name = "Profile"
      let profileViewController = StoryboardViewControllerResource<ProfileViewController>(identifier: "ProfileViewController")

      func profileViewController(_: Void = ()) -> ProfileViewController? {
        return UIKit.UIStoryboard(resource: self).instantiateViewController(withResource: profileViewController)
      }

      static func validate() throws {
        if UIKit.UIImage(named: "profile", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Image named 'profile' is used in storyboard 'Profile', but couldn't be loaded.") }
        if #available(iOS 11.0, tvOS 11.0, *) {
          if UIKit.UIColor(named: "MainColor", in: R.hostingBundle, compatibleWith: nil) == nil { throw Rswift.ValidationError(description: "[R.swift] Color named 'MainColor' is used in storyboard 'Profile', but couldn't be loaded.") }
        }
        if _R.storyboard.profile().profileViewController() == nil { throw Rswift.ValidationError(description:"[R.swift] ViewController with identifier 'profileViewController' could not be loaded from storyboard 'Profile' as 'ProfileViewController'.") }
      }

      fileprivate init() {}
    }
    #endif

    fileprivate init() {}
  }
  #endif

  fileprivate init() {}
}
