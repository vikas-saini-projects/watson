//
//  ProfileViewController.swift
//  Watson
//
//  Created by Techwin on 17/02/21.
//

import UIKit

class ProfileViewController: UIViewController {

    //MARK:- OUTLETS
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var BottomConstaint: NSLayoutConstraint!
    @IBOutlet weak var transparentView: UIView!
    @IBOutlet weak var InformationView: UIView!
    @IBOutlet weak var informationViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var DescriptionLabel: UILabel!
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    
    
    //MARK:- VARIABLES AND CLASS CONSTANTS
    var sendhideMessageDelegate : HideProfileMenuProtocol? = nil
    var ImagesArray = [#imageLiteral(resourceName: "rating"),#imageLiteral(resourceName: "terms-and-conditions"),#imageLiteral(resourceName: "privacy-policy"),#imageLiteral(resourceName: "login")]
    var TextArray = ["Rate Us"  , "Terms & Conditions", "Privacy Policy" , "Login"]
    
    
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //notification
        NotificationCenter.default.addObserver(self, selector: #selector(AuthChanged), name: NSNotification.Name.init("auth"), object: nil)
        GestureAddtion() // add gestures to transparent view
        //tableView setup
        self.tableView.register(UINib(nibName: R.reuseIdentifier.tableViewCellForProfle.identifier, bundle: nil), forCellReuseIdentifier: R.reuseIdentifier.tableViewCellForProfle.identifier)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .none
    }
    
    
    //MARK:- LOGIN / LOGOUT STATE CHANGE
    @objc func AuthChanged(_ notification : Notification) {
        PrintToConsole("recieved notification")
        loginOrLogoutUI()
    }
    
    
    //MARK:- VIEW WILL APPEAR
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loginOrLogoutUI()
    }
    
    //MARK:- ADD SWIPE AND TAP GESTURE ON TRANSPARENT VIEW
    func GestureAddtion(){
        //swipe
        let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(sender:)))
        swipeGesture.direction = [.left , .right]
        self.transparentView.addGestureRecognizer(swipeGesture)
        //tap
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.handleTapGesture(sender:)))
        self.transparentView.addGestureRecognizer(gesture)
    }
    
    //MARK:- DETECT SWIPE GESTURE
    @objc func handleSwipe(sender: UISwipeGestureRecognizer) {
        sendhideMessageDelegate?.HideSideMenu()
    }
    
    //MARK:- DETECT TAP GESTURES
    @objc func handleTapGesture(sender : UITapGestureRecognizer) {
        sendhideMessageDelegate?.HideSideMenu()
    }
    
    //MARK:- VIEW DID LAYOUT SUBVIEW
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        DispatchQueue.main.async {
            self.InformationView.giveShadowAndRoundCorners(shadowOffset: CGSize.zero, shadowRadius: 05, opacity: 0.8, shadowColor: R.color.mainColor() ?? MAIN_COLOR, cornerRadius: 0)
            self.informationViewWidthConstraint.constant = UIScreen.main.bounds.width * 0.6 // setting width of information view
        }
        //setup top and bottom constraints
        let bottom = self.view.safeAreaInsets.bottom
        let top = self.view.safeAreaInsets.top
        self.BottomConstaint.constant = 05.0 + bottom
        self.topConstraint.constant = 05.0 + top
        DispatchQueue.main.async {
            self.profileImageView.layer.cornerRadius = self.profileImageView.bounds.height / 2
            self.profileImageView.clipsToBounds = true
        }
        
    }
    
    
    //MARK:- LOGIN / LOGOUT BUTTON
    func loginOrLogoutUI(){
        DispatchQueue.main.async {
        if UserDefaults.standard.integer(forKey: UD_USERID) != 0 {
            //user is logged in
            self.TextArray[3] = "Logout"
            self.ImagesArray[3] = #imageLiteral(resourceName: "logout")
            self.profileImageView.sd_setImage(with: URL(string: UserDefaults.standard.string(forKey: UD_IMAGEURL) ?? "https://nourl.com"), placeholderImage: #imageLiteral(resourceName: "profile"), options: [.highPriority], context: nil)
            self.NameLabel.text = UserDefaults.standard.string(forKey: UD_NAME)
            self.DescriptionLabel.text = UserDefaults.standard.string(forKey: UD_EMAIL)
        }else {
            self.TextArray[3] = "Login"
            self.ImagesArray[3] = #imageLiteral(resourceName: "login")
            self.NameLabel.text = ""
            self.DescriptionLabel.text = "Login to like and bookmark your favorite news posts."
            self.profileImageView.image = #imageLiteral(resourceName: "profile")
        }
        }
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
