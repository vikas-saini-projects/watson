//
//  Profile + tableView.swift
//  Watson
//
//  Created by Techwin on 17/02/21.
//

import Foundation
import UIKit
import StoreKit


//MARK:- EXTENSION FOR TABLE VIEW ON PROFILE PAGE

extension ProfileViewController : UITableViewDelegate , UITableViewDataSource {
    
    //MARK:- NUBMER OF ROWS
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    //MARK:- SETUP A TABLE VIEW CELL
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.tableViewCellForProfle , for: indexPath) {
            cell.imageForview.image = ImagesArray[indexPath.row]
            cell.LabelForText.text = TextArray[indexPath.row]
            cell.selectionStyle = .none
            // CHANGE COLOR IF IT IS LAST CELL
            if indexPath.row == 3 {
                cell.imageForview.tintColor = R.color.mainColor()
                cell.LabelForText.textColor = R.color.mainColor()
            }else{
                if #available(iOS 13.0, *) {
                    cell.imageForview.tintColor = UIColor.label
                    cell.LabelForText.textColor = UIColor.label
                } else {
                    cell.imageForview.tintColor = UIColor.black
                    cell.LabelForText.textColor = UIColor.black
                }
            }
            return cell
        }
        return UITableViewCell()
    }
    
    //MARK:- DID SELECT A CELL OF TABLE VIEW
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            //rate us
            SKStoreReviewController.requestReview()
        }else if indexPath.row == 3 {
            //login or logout
            loginOrLogout()
        }
    }
    
    //MARK:- SET HEIGHT OF TABLE VIEW CELL
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    //MARK:- LOGIN OR LOGOUT FUNCTIONALITY
    func loginOrLogout(){
        if UserDefaults.standard.integer(forKey: UD_USERID) == 0 {
        if let AuthVc = R.storyboard.authentication.authViewController() {
            self.present(AuthVc, animated: true, completion: nil)
        }
        }else{
           DeleteWholeUserDefaults()
        }
    }
    
    
    //MARK:- DELETE WHOLE USERDEFAUTLS
    func DeleteWholeUserDefaults(){
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        NotificationCenter.default.post(name: NSNotification.Name.init("auth"), object: nil)
    }
}
