//
//  ResourceModel.swift
//  Watson
//
//  Created by Techwin on 24/02/21.
//

import Foundation

// MARK: - AllSourcesModel
struct AllSourcesModel: Codable {
    var status: Int
    var message: String
    var data: [AllSourcesModelDataClass]?
    var method, success: String?
}

// MARK: - AllSourcesModelDataClass
struct AllSourcesModelDataClass: Codable {
    var resource_id, category_id, resource_name: String?
    var resource_url: String?
    var xml_url: String?
    var resource_image : String?
    var isactive, created_at, updated_at: String?

}
