//
//  Home + CollectionView.swift
//  Watson
//
//  Created by Techwin on 16/02/21.
//

import Foundation
import UIKit


//MARK:- EXTENSTION FOR COLLECTION VIEW


extension HomeViewController : UICollectionViewDelegate ,  UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isSourcesSearched{
        return self.SearchedSourcesData.count
        }else {
        return self.allSourcesData.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier:R.reuseIdentifier.collectionViewCellForHome, for: indexPath) {
        if isSourcesSearched {
            cell.populateData(SearchedSourcesData[indexPath.row])
        }else {
        cell.populateData(allSourcesData[indexPath.row])
            }
        return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.width - 20 ) / 3, height: (collectionView.bounds.width - 20 ) / 3)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let feedVc = R.storyboard.feeds.feedsViewController() {
            feedVc.sourceId = isSourcesSearched ? Int(SearchedSourcesData[indexPath.row].resource_id ?? "0") ?? 0 : Int(allSourcesData[indexPath.row].resource_id ?? "0") ?? 0
            self.navigationController?.pushViewController(feedVc, animated: true)
        }
    }
}
