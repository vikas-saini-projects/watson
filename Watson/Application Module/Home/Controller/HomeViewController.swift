//
//  HomeViewController.swift
//  Watson
//
//  Created by Techwin on 16/02/21.
//

import UIKit

class HomeViewController: UIViewController {

    //MARK:- OUTLETS
    @IBOutlet weak var heightConstraintForTopView: NSLayoutConstraint!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tfSearch: UITextField!
    
    //MARK:- VARIABLES
    var allSourcesData = [AllSourcesModelDataClass]()
    var SearchedSourcesData = [AllSourcesModelDataClass]()
    var isSourcesSearched = false
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
    super.viewDidLoad()
        //collectionView
        collectionView.register(UINib(nibName: R.reuseIdentifier.collectionViewCellForHome.identifier, bundle: nil), forCellWithReuseIdentifier: R.reuseIdentifier.collectionViewCellForHome.identifier)
       //api call
        self.apiCallForGetSources()
      
        //search tf
        self.tfSearch.addTarget(self, action: #selector(SearchTextChanging), for: UIControl.Event.editingChanged)
    }
    
    //MARK:- SEARCH TEXT CHANGING
    @objc func SearchTextChanging(_ tf : UITextField) {
        SearchedSourcesData = tf.text!.isEmpty ? allSourcesData : (allSourcesData.filter{($0.resource_url?.range(of: tf.text!, options: [.caseInsensitive]) != nil) || ($0.resource_name?.range(of: tf.text! , options: [.caseInsensitive]) != nil )
        })
        if tf.text == "" {
            isSourcesSearched = false
        }
        
        if SearchedSourcesData.count>0 {
            isSourcesSearched = true
        }else{
            isSourcesSearched  = true
            
        }
        DispatchQueue.main.async {
        self.collectionView.reloadData()
        }
    }
    
    
    //MARK:- VIEW DID LAYOUT SUBVIEW
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setTopView()
    }
    
    
    //MARK:- SET TOP VIEW
    func setTopView(){
        //top view height
        let safeAreaValue = self.view.safeAreaInsets.top
        self.heightConstraintForTopView.constant = safeAreaValue + 15 + self.searchView.bounds.height + 15    //  height of searchView , 10 bottom , 10 top to safe area
        // round corners
        self.searchView.roundViewCorner(radius: self.searchView.bounds.height / 2) //divide height by 2 to make it round
        
    }

    
    
    //MARK:- BUTTON ACTIONS
    @IBAction func ActionForSearchButton(_ sender: Any) {
        //This button is hidden now, Currently no need
    }
    
    @IBAction func ActionForBookmarkButton(_ sender: Any) {
        if UserDefaults.standard.integer(forKey: UD_USERID ) != 0 {
            //user is already logged in
            if let feedVc = R.storyboard.feeds.feedsViewController() {
                feedVc.isForAllBookmarkedNews = true
                self.navigationController?.pushViewController(feedVc, animated: true)
            }
        }else {
            //NOT LOGGED IN
            if let AuthVc = R.storyboard.authentication.authViewController() {
                self.present(AuthVc, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func ActionForEditorChoice(_ sender: Any) {
        if let feedVc = R.storyboard.feeds.feedsViewController() {
            feedVc.isForAllEditorChoiceNews = true
            self.navigationController?.pushViewController(feedVc, animated: true)
        }
    }
    
}
