//
//  Home + Api.swift
//  Watson
//
//  Created by Techwin on 24/02/21.
//

import Foundation

extension HomeViewController {
    
    
    func apiCallForGetSources(){
        
        startAnimating(self.view)
        ApiManager.shared.Request(type: AllSourcesModel.self, methodType: .Post, url: BASE_URL + SOURCES_API, parameter: [:]) { (error, response, messageString, statusCode) in
            if statusCode == 200 {
                if error != nil {
                    //error while decoding
                    Toast.show(message: messageString ?? error?.localizedDescription ?? "Something Went Wrong!", controller: self, color: R.color.mainColor() ?? MAIN_COLOR)
                }else {
                    if let data = response?.data {
                        self.allSourcesData = data
                        
                        if self.allSourcesData.count > 0 {
                            
                        for i in 0...self.allSourcesData.count - 1 {
                            self.allSourcesData[i].resource_name = "Source \(i)"
                        }
                            
                        DispatchQueue.main.async {
                        self.collectionView.delegate = self
                        self.collectionView.dataSource = self
                        self.collectionView.reloadData()
                        }
                        }
                    }else {
                        PrintToConsole("No Data Found for resources api")
                    }
                }
            }else {
                Toast.show(message: messageString ?? error?.localizedDescription ?? "Something Went Wrong!", controller: self, color: R.color.mainColor() ?? MAIN_COLOR)
            }
        }
    }
    
    
}
