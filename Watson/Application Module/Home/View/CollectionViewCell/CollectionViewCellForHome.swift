//
//  CollectionViewCellForHome.swift
//  Watson
//
//  Created by Techwin on 16/02/21.
//

import UIKit
import SDWebImage

class CollectionViewCellForHome: UICollectionViewCell {

    
    //MARK:- OUTLETS
    @IBOutlet weak var labelForText: UILabel!
    @IBOutlet weak var Imageview: UIImageView!
    @IBOutlet weak var BaseView: UIView!
    @IBOutlet weak var viewForShadow: UIView!
    

    //MARK:-  AWAKE FROM NIB
    override func awakeFromNib() {
        super.awakeFromNib()
        self.BaseView.roundViewCorner(radius: 10)
        self.viewForShadow.giveShadowAndRoundCorners(shadowOffset: CGSize(width: 0, height: 5), shadowRadius: 20, opacity: 0.8, shadowColor: R.color.mainColor() ?? MAIN_COLOR, cornerRadius: 00)
    }
    
    
    //MARK:- POPULATING DATA ON CELLS
    func populateData(_ source : AllSourcesModelDataClass?) {
        
        Imageview.image = #imageLiteral(resourceName: "SourceImage")
        
// Image is static , client want this , so below two lines are static
// Imageview.sd_imageIndicator = SDWebImageActivityIndicator.gray
// Imageview.sd_setImage(with: URL(string: BASE_URL + (source?.resource_image ?? ""))!, placeholderImage: #imageLiteral(resourceName: "SourceImage"), options: [.highPriority], context: nil)
        
        labelForText.text = source?.resource_name ?? ""
    }

    
    
    
}
