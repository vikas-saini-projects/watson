//
//  LoginModel.swift
//  Watson
//
//  Created by Techwin on 24/02/21.
//

import Foundation


// MARK: - LoginModel
struct LoginModel: Codable {
    let status: Int?
    let message: String?
    let data: LoginModelDataClass?
    let method, success: String?
}

// MARK: - LoginModelDataClass
struct LoginModelDataClass: Codable {
    let user_id, social_id, name, email: String?
    let session_key, device_token, login_type, device_type: String?
    let isactive, created_at: String?
    let updated_at: String?
}

