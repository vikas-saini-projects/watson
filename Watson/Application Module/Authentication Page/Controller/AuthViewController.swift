//
//  AuthViewController.swift
//  Watson
//
//  Created by Techwin on 17/02/21.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn

class AuthViewController: UIViewController {

    //MARK:- OUTLETS
    @IBOutlet weak var GoogleView: UIView!
    @IBOutlet weak var facebookView: UIView!
    @IBOutlet weak var linkTextView: UITextView!
    @IBOutlet weak var transparentView: UIView!
    @IBOutlet weak var informationView: UIView!
    @IBOutlet weak var BottomConstraint: NSLayoutConstraint!
    
    
    //MARK- CLASS VARIABLES
    let loginManager = LoginManager()
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        //Google Sign In
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.delegate = self
        //Logging out google everytime when user see this screen,
        if GIDSignIn.sharedInstance()?.currentUser == nil{}else {
            GIDSignIn.sharedInstance()?.signOut()
        }
        //Logging out facebook everytime when user see this screen , purely to avoid conflictions in testing
        loginManager.logOut()
        //setup underlines and links
        setupLinksAndUnderline()
        //TAP GESTURE ON TRANSPARENT VIEW
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.handleTapGesture(sender:)))
        self.transparentView.addGestureRecognizer(gesture)
    }
    
    //MARK:- DETECT TAP GESTURES
    @objc func handleTapGesture(sender : UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK:- SETUP UNDERLINE AND LINKS
    func setupLinksAndUnderline(){
        linkTextView.isUserInteractionEnabled = true
        linkTextView.isEditable = false
        let str  = "By signing up you agree to our Privacy Policy and Terms & Condition" as NSString
        let attributedString = NSMutableAttributedString(string: str as String)
        attributedString.addAttribute(.link, value: "http://www.google.fr", range:str.range(of:"Terms & Condition")
        )
        attributedString.addAttribute(.link, value: "http://www.google.fr", range: str.range(of: "Privacy Policy"))
        let blueColor = UIColor(red: 0/255, green: 117/255, blue: 187/255, alpha: 1)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: blueColor, range: NSRange.init(location:0, length: str.length))
        
        linkTextView.linkTextAttributes = [
            NSAttributedString.Key.foregroundColor: blueColor,
            NSAttributedString.Key.underlineStyle: NSNumber(value: NSUnderlineStyle.single.rawValue)
        ]
        linkTextView.attributedText = attributedString
        linkTextView.textAlignment = .center
        linkTextView.linkTextAttributes = [.underlineStyle: NSUnderlineStyle.single.rawValue]
        linkTextView.textColor = UIColor.white
    }

    //MARK:- VIEW DID LAYOUT SUBVIEW
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let bottom = self.view.safeAreaInsets.bottom
        self.BottomConstraint.constant = 10.0 + bottom
        
        DispatchQueue.main.async {
            self.facebookView.roundViewCorner(radius: self.facebookView.layer.bounds.height / 2)
            self.GoogleView.roundViewCorner(radius: self.GoogleView.layer.bounds.height / 2)
        }
    }

    
    //MARK:- ACTIONS FOR BUTTONS
    
    @IBAction func ActionForGoogleButton(_ sender: Any) {
        if GIDSignIn.sharedInstance()?.currentUser == nil {
            GIDSignIn.sharedInstance().signIn()
        }
        else {
            GIDSignIn.sharedInstance()?.signOut()
        }
    }
    
    @IBAction func ActionForFacebookButton(_ sender: Any) {
        facebookLogin()
    }
    
    
    
    //MARK:- FACEBOOK LOGIN FUNCTION
    func facebookLogin(){
        
        if(AccessToken.current == nil ) {
            loginManager.logIn(permissions: ["public_profile", "email"], from: self) { (result, error) in
                if ((result?.isCancelled)!) {
                }
                else {
                    if (error == nil) {
                        let params = ["fields" : "id, name, first_name, last_name, picture.type(large), email "]
                        let graphRequest = GraphRequest.init(graphPath: "/me", parameters: params)
                        let Connection = GraphRequestConnection()
                        Connection.add(graphRequest) { (Connection, result, error) in
                            let info = result as! [String : AnyObject]
                            let picDict = info["picture"] as? [String : Any ]
                            let dataDict = picDict!["data"] as! [String:Any]
                            let PictureUrl = dataDict["url"] as? String
                            let firstname = info["first_name"] as? String ?? ""
                            let lastname = info["last_name"] as? String ?? ""
                            let socialID = info["id"] as? String ?? ""
                            let email = info["email"] as? String ?? ""
                            
                            //saving image
                            UserDefaults.standard.set(PictureUrl, forKey: UD_IMAGEURL)
                            self.apiCallForLogin(loginType: LoginType.Facebook.rawValue, name: firstname  + " " + lastname , email: email, Social_Id: socialID)
                            
                        }
                        Connection.start()
                    }
                }
                if let error = error {
                    let alertController = UIAlertController(title: "Message", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                    return
                }
            }
        }else {
            loginManager.logOut()
            let alertController = UIAlertController(title: "Message", message: "Logged Out", preferredStyle: UIAlertController.Style.alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
}

