//
//  Auth + GoogleSignIn.swift
//  Watson
//
//  Created by Techwin on 22/02/21.
//

import Foundation
import GoogleSignIn


extension AuthViewController : GIDSignInDelegate {
    
    //MARK:- GOOGLE SIGN IN
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            if GIDSignIn.sharedInstance()?.currentUser != nil{
               
                let name = user.profile.name ?? "--"
                var imageUrl = "no Url"
                if  user.profile.hasImage {
                    imageUrl = user.profile.imageURL(withDimension: .max)?.absoluteString ?? "someUrl"
                    UserDefaults.standard.set(imageUrl, forKey: UD_IMAGEURL)
                }
                let email = user.profile.email!
                let socialID = user.userID!
                self.apiCallForLogin(loginType: LoginType.Google.rawValue, name: name, email: email, Social_Id: socialID)
            }
            else{
                print("error signing in")
            } 
        }
        else {
            print("\(error.localizedDescription)")
        }
    }
    
}
