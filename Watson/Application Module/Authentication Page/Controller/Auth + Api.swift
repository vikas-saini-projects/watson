//
//  Auth + Api.swift
//  Watson
//
//  Created by Techwin on 24/02/21.
//

import Foundation

extension AuthViewController {
    
    
    func apiCallForLogin(loginType: Int , name : String , email: String , Social_Id : String){
        startAnimating(self.view)
        let params = [
            "social_id" : Social_Id ,
            "device_type": DEVICE_TYPE,
            "device_token": DEVICE_TOKEN,
            "login_type": loginType,
            "email": email,
            "name":name] as [String:Any]
        ApiManager.shared.Request(type: LoginModel.self, methodType: .Post, url: BASE_URL + LOGIN_API, parameter: params) { (error, response, messageString, statusCode) in
            if statusCode == 200 {
                if error != nil {
                    //error while decoding
                    Toast.show(message: messageString ?? error?.localizedDescription ?? "Something Went Wrong!", controller: self, color: R.color.mainColor() ?? MAIN_COLOR)
                }else {
                    if let data = response?.data {
                        UserDefaults.standard.set(data.user_id?.toInt(), forKey: UD_USERID)
                        UserDefaults.standard.set(data.name, forKey: UD_NAME)
                        UserDefaults.standard.set(data.email, forKey: UD_EMAIL)
                        NotificationCenter.default.post(name: NSNotification.Name.init("auth"), object: nil)
                        DispatchQueue.main.async {
                        self.dismiss(animated: true, completion: nil)
                        }
                    }else {
                        PrintToConsole("No Data Found for resources api")
                    }
                }
            }else {
                Toast.show(message: messageString ?? error?.localizedDescription ?? "Something Went Wrong!", controller: self, color: R.color.mainColor() ?? MAIN_COLOR)
            }
        }
    }
    
    
}
