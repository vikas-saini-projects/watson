//
//  FeedsViewController.swift
//  Watson
//
//  Created by Techwin on 17/02/21.
//

import UIKit

class FeedsViewController: UIViewController {
    
    //MARK:- OUTLETS
    @IBOutlet weak var BottomView: UIView!
    @IBOutlet weak var refreshLabel: UILabel!
    @IBOutlet weak var refreshImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var ContainerViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    
    //MARK:- VARIABLES
    var sourceId = 0
    var pageNo = 1
    var allNewsData = [AllNewsModelDataClass]()
    var allNewsMeta : AllNewsModelMetaDataClass?
    var isForAllBookmarkedNews = false //if true , will show all the bookmarked news of this user
    var isForAllEditorChoiceNews = false // if true , will show all the Editor choice News
    
    
    //MARK:- VIEW DID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        //without these 3 lines, table view distorted on pagination
        tableView.estimatedRowHeight = 0
        tableView.estimatedSectionHeaderHeight = 0
        tableView.estimatedSectionFooterHeight = 0
        //Notification observer
        NotificationCenter.default.addObserver(self, selector: #selector(UserStateChanged), name: NSNotification.Name.init("auth"), object: nil)
        self.tableView.separatorStyle = .none // show no separator
        self.ContainerViewLeadingConstraint.constant = UIScreen.main.bounds.width // it will push the container view out of sight by giving its leading constraint a value equal to width of screen
        self.attachProfileViewController() //attaching profile View Controller
        //tableView datasource
        self.tableView.register(UINib(nibName: R.reuseIdentifier.tableViewCellForFeed.identifier, bundle: nil), forCellReuseIdentifier: R.reuseIdentifier.tableViewCellForFeed.identifier)
        //refresh or top button
        self.refreshLabel.text = "Refresh"
        self.refreshImage.image = R.image.refresh()
        //apiCall
        if isForAllBookmarkedNews {
            //showing all Bookmarked News
            self.apiCallForGetAllBookMarkedNews()
        }else if isForAllEditorChoiceNews {
            //showing all Editor choice News
            self.apiCallForGetAllEditorChoiceNews()
        }else {
            //showing news of a particular resource
            self.apiCallForGetNewsOFParticularSources()
        }
    }
    
        
        //MARK:- USER STATE CHANGED
        //IT WILL BE CALLED WHEN USER LOGIN OR LOGOUT
        @objc func UserStateChanged(_ notification : Notification){
            if isForAllBookmarkedNews {
                //showing all Bookmarked News
                DispatchQueue.main.async {
                self.navigationController?.popViewController(animated: true)
                }
            }else if isForAllEditorChoiceNews {
                //showing all Editor choice News
                self.allNewsData.removeAll()//first remove every News from page
                pageNo = 1 //Get first page and then get all EditorChoice News
                self.apiCallForGetAllEditorChoiceNews()
            }else {
                //showing news of a particular resource
                self.allNewsData.removeAll()//first remove every News from page
                pageNo = 1 //Get first page and then get all News
                self.apiCallForGetNewsOFParticularSources()
            }
        }
        
        //MARK:- VIEW DID LAYOUT SUBVIEW
        override func viewDidLayoutSubviews() {
            super.viewDidLayoutSubviews()
            DispatchQueue.main.async {
                self.BottomView.giveShadowAndRoundCorners(shadowOffset: CGSize.zero, shadowRadius: 10, opacity: 0.4, shadowColor: R.color.mainColor() ?? MAIN_COLOR, cornerRadius: self.BottomView.layer.bounds.height / 2)
            }
        }
        
        //MARK:- ADD PROFILE VIEW CONTROLLER AS CHILD
        func attachProfileViewController(){
            //Remove Before adding profile vc as child
            self.removeChild()
            //Now add profile view controller as child
            if let profileVC = R.storyboard.profile.profileViewController() {
                profileVC.sendhideMessageDelegate = self
                self.addChildView(viewController: profileVC, in: self.containerView)
            }
        }
        
        
        //MARK:- BUTTON ACTIONS
        @IBAction func ActionForSourcesButton(_ sender: Any) {
            self.navigationController?.popViewController(animated: true)
        }
        
        
        @IBAction func ActionForRefreshOrTopButton(_ sender: Any) {
            if self.refreshLabel.text == "Top" {
                // this button take it to first cell
                self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableView.ScrollPosition.top, animated: true)
            } else {
                // it is refresh Button
                self.allNewsData.removeAll()//first remove every News from page
                pageNo = 1 //Get first page and then get all News
                self.apiCallForGetNewsOFParticularSources()
            }
        }
        
        @IBAction func ActionForProfileButton(_ sender: Any) {
            //on this Button Click, we will pull the container view with animation
            self.ContainerViewLeadingConstraint.constant = -(self.ContainerViewLeadingConstraint.constant)
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            
        }
        
    
}
    
    
    //MARK:- EXTENSION FOR HIDE SIDE MENU PROTOCOL
    extension FeedsViewController : HideProfileMenuProtocol {
        func HideSideMenu() {
            self.ContainerViewLeadingConstraint.constant = -(self.ContainerViewLeadingConstraint.constant)// it will push the container view out of sight by giving its leading constraint a value equal to width of screen
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
