//
//  Feed + Api.swift
//  Watson
//
//  Created by Techwin on 24/02/21.
//

import Foundation
import UIKit

extension FeedsViewController {
    
    //MARK:- GET NEWS OF A PARTICUALR SOURCE
    func apiCallForGetNewsOFParticularSources(){
        
        //Only show loader if it is first page to be loaded
        if pageNo == 1 {
            DispatchQueue.main.async {
            startAnimating(self.view)
            }
        }
        var params = ["resource_id": self.sourceId , "page_no": pageNo]
        if UserDefaults.standard.integer(forKey: UD_USERID) != 0 {
            params["user_id"] =  UserDefaults.standard.integer(forKey: UD_USERID)
        }
        ApiManager.shared.Request(type: AllNewsModel.self, methodType: .Get, url: BASE_URL + NEWS_API, parameter: params) { (error, response, messageString, statusCode) in
            if statusCode == 200 {
                if error != nil {
                    //error while decoding
                    Toast.show(message: messageString ?? error?.localizedDescription ?? "Something Went Wrong!", controller: self, color: R.color.mainColor() ?? MAIN_COLOR)
                }else {
                    //getting data
                    if let data = response?.data {
                        DispatchQueue.main.async {
                        for particularNews in data {
                        if !self.allNewsData.contains(particularNews) {
                        self.allNewsData.append(particularNews)
                        }
                        }
                        }
                        DispatchQueue.main.async {
                        
                        self.tableView.delegate = self
                        self.tableView.dataSource = self
                        self.tableView.reloadData()
                        }
                    }else {
                        PrintToConsole("No Data Found for resources api")
                    }
                    //getting meta
                    if let meta = response?.meta_data {
                        self.allNewsMeta = meta
                    }else {
                        PrintToConsole("No Meta Found")
                    }
                }
            }else {
                Toast.show(message: messageString ?? error?.localizedDescription ?? "Something Went Wrong!", controller: self, color: R.color.mainColor() ?? MAIN_COLOR)
            }
        }
    }
    
    
    //MARK:- LIKE A NEWS
    func apiCallForLikeNews(newsID : Int , Status : Int , index: Int){
        
        startAnimating(self.view)
        let params = ["user_id": UserDefaults.standard.integer(forKey: UD_USERID) , "news_id": newsID , "status": Status]
        ApiManager.shared.Request(type: LikeUnlikeModel.self, methodType: .Post, url: BASE_URL + LIKE_UNLIKE_API, parameter: params) { (error, response, messageString, statusCode) in
            if statusCode == 200 {
                if error != nil {
                    //error while decoding
                    Toast.show(message: messageString ?? error?.localizedDescription ?? "Something Went Wrong!", controller: self, color: R.color.mainColor() ?? MAIN_COLOR)
                }else {
                    if (response?.data) != nil {
                        if Status == 1 {
                            //liked
                            self.allNewsData[index].liked! += 1
                            self.allNewsData[index].is_liked = 1
                        }else {
                            //desliked
                            self.allNewsData[index].liked! -= 1
                            self.allNewsData[index].is_liked = 0
                        }
                        DispatchQueue.main.async {
                            self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: UITableView.RowAnimation.automatic)
                        }
                    }else {
                        PrintToConsole("No Data Found for resources api")
                    }
                }
            }else {
                Toast.show(message: messageString ?? error?.localizedDescription ?? "Something Went Wrong!", controller: self, color: R.color.mainColor() ?? MAIN_COLOR)
            }
        }
    }
    
    //MARK:- BOOKMARK A NEWS
    func apiCallForBookmarkNews(newsID : Int , index: Int , isBookMarked: Int){
        
        startAnimating(self.view)
        let params = ["user_id": UserDefaults.standard.integer(forKey: UD_USERID) , "news_id": newsID ]
        ApiManager.shared.Request(type: LikeUnlikeModel.self, methodType: .Post, url: BASE_URL + BOOKMARK_API, parameter: params) { (error, response, messageString, statusCode) in
            if statusCode == 200 {
                if error != nil {
                    //error while decoding
                    Toast.show(message: messageString ?? error?.localizedDescription ?? "Something Went Wrong!", controller: self, color: R.color.mainColor() ?? MAIN_COLOR)
                }else {
                        if isBookMarked == 0 {
                            //it was not bookmarked, now it is bookmarked
                            self.allNewsData[index].is_bookmarked = 1
                            Toast.show(message: "Added to Bookmarks...", controller: self , color: MAIN_COLOR)
                        }else {
                            //it was bookmarked , now it is removed from bookmarks
                            self.allNewsData[index].is_bookmarked = 0
                            Toast.show(message: "Remove from Bookmarks...", controller: self , color:  MAIN_COLOR)
                        }
                        DispatchQueue.main.async {
                            self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: UITableView.RowAnimation.automatic)
                        }
                }
            }else {
                Toast.show(message: messageString ?? error?.localizedDescription ?? "Something Went Wrong!", controller: self, color: R.color.mainColor() ?? MAIN_COLOR)
            }
        }
    }
    
    //MARK:- ALL BOOKMARKED NEWS
    func apiCallForGetAllBookMarkedNews(){
        //Only show loader if it is first page to be loaded
        if pageNo == 1 {
            DispatchQueue.main.async {
            startAnimating(self.view)
            }
        }
        var params = ["user_id": UserDefaults.standard.integer(forKey: UD_USERID) , "page_no": pageNo]
        ApiManager.shared.Request(type: AllNewsModel.self, methodType: .Post, url: BASE_URL + GET_BOOKMARKED_API, parameter: params) { (error, response, messageString, statusCode) in
            if statusCode == 200 {
                if error != nil {
                    //error while decoding
                    Toast.show(message: messageString ?? error?.localizedDescription ?? "Something Went Wrong!", controller: self, color: R.color.mainColor() ?? MAIN_COLOR)
                }else {
                    //getting data
                    if let data = response?.data {
                        DispatchQueue.main.async {
                        for particularNews in data {
                        if !self.allNewsData.contains(particularNews) {
                        self.allNewsData.append(particularNews)
                        }
                        }
                        self.tableView.delegate = self
                        self.tableView.dataSource = self
                        self.tableView.reloadData()
                        }
                    }else {
                        PrintToConsole("No Data Found for resources api")
                    }
                    //getting meta
                    if let meta = response?.meta_data {
                        self.allNewsMeta = meta
                    }else {
                        PrintToConsole("No Meta Found")
                    }
                }
            }else {
                Toast.show(message: messageString ?? error?.localizedDescription ?? "Something Went Wrong!", controller: self, color: R.color.mainColor() ?? MAIN_COLOR)
            }
        }
    }
    
    
    //MARK:- ALL EDITOR CHOICE NEWS
    func apiCallForGetAllEditorChoiceNews(){
        //Only show loader if it is first page to be loaded
        if pageNo == 1 {
            DispatchQueue.main.async {
            startAnimating(self.view)
            }
        }
        var params = ["page_no": pageNo]
        if UserDefaults.standard.integer(forKey: UD_USERID) != 0 {
            params["user_id"] =  UserDefaults.standard.integer(forKey: UD_USERID)
        }
        ApiManager.shared.Request(type: AllNewsModel.self, methodType: .Post, url: BASE_URL + GET_EDITORCHOICE_API, parameter: params) { (error, response, messageString, statusCode) in
            if statusCode == 200 {
                if error != nil {
                    //error while decoding
                    Toast.show(message: messageString ?? error?.localizedDescription ?? "Something Went Wrong!", controller: self, color: R.color.mainColor() ?? MAIN_COLOR)
                }else {
                    //getting data
                    if let data = response?.data {
                        DispatchQueue.main.async {
                        for particularNews in data {
                        if !self.allNewsData.contains(particularNews) {
                        self.allNewsData.append(particularNews)
                        }
                 
                   
                        self.tableView.delegate = self
                        self.tableView.dataSource = self
                            self.tableView.layoutIfNeeded()
                        self.tableView.reloadData()
    
                        }
                        }
                    }else {
                        PrintToConsole("No Data Found for resources api")
                    }
                    //getting meta
                    if let meta = response?.meta_data {
                        self.allNewsMeta = meta
                    }else {
                        PrintToConsole("No Meta Found")
                    }
                }
            }else {
                Toast.show(message: messageString ?? error?.localizedDescription ?? "Something Went Wrong!", controller: self, color: R.color.mainColor() ?? MAIN_COLOR)
            }
        }
    }
    
}
