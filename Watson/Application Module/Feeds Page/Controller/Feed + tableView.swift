//
//  Feed + tableView.swift
//  Watson
//
//  Created by Techwin on 17/02/21.
//

import Foundation
import UIKit

extension FeedsViewController : UITableViewDelegate , UITableViewDataSource {
    
    
    //MARK:- NUMBER OF ROWS
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allNewsData.count
    }
    
    //MARK:- SETUP A CELL
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.tableViewCellForFeed.identifier, for: indexPath) as? TableViewCellForFeed {
            cell.populateData(self.allNewsData[indexPath.row])
            cell.likeButton.tag = indexPath.row
                cell.likeButton.addTarget(self, action: #selector(self.LikeTapped), for: UIControl.Event.touchUpInside)
            cell.bookMarkButton.tag  = indexPath.row
                cell.bookMarkButton.addTarget(self, action: #selector(self.BookmarKTapped), for: UIControl.Event.touchUpInside)
            cell.readFullButton.tag = indexPath.row
                cell.readFullButton.addTarget(self, action : #selector(self.readFullTapped), for: UIControl.Event.touchUpInside)
            cell.shareButton.tag = indexPath.row
                cell.shareButton.addTarget(self, action: #selector(self.shareTapped), for: UIControl.Event.touchUpInside)
    
            return cell
        }
        return UITableViewCell()
    }
    
    //MARK:- DECIDE HEIGHT OF A CELL
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        return tableView.bounds.height
    }
    

    //MARK:- LIKE TAPPED
    @objc func LikeTapped(_ sender : UIButton) {
        //NEED TO CHECK IF USER IS LOGGED IN OR NOT
        if UserDefaults.standard.integer(forKey: UD_USERID) == 0 {
            //NOT LOGGED IN
            if let AuthVc = R.storyboard.authentication.authViewController() {
                self.present(AuthVc, animated: true, completion: nil)
            }
        }else {
            //USER IS LOGGED IN ALREADY , GIVE THEM RIGHT TO LIKE THIS NEWS
            self.apiCallForLikeNews(newsID: (allNewsData[sender.tag].news_id ?? "0").toInt() ?? 0, Status: allNewsData[sender.tag].is_liked == 0 ? 1 : 0, index: sender.tag)
        }
     
    }
    
    
    //MARK:- BOOKMARK TAPPED
    @objc func BookmarKTapped(_ sender : UIButton) {
        //NEED TO CHECK IF USER IS LOGGED IN OR NOT
        if UserDefaults.standard.integer(forKey: UD_USERID) == 0 {
            //NOT LOGGED IN
            if let AuthVc = R.storyboard.authentication.authViewController() {
                self.present(AuthVc, animated: true, completion: nil)
            }
        }else {
            //USER IS LOGGED IN ALREADY , GIVE THEM RIGHT TO LIKE THIS NEWS
            apiCallForBookmarkNews(newsID: (allNewsData[sender.tag].news_id ?? "0").toInt() ?? 0, index: sender.tag, isBookMarked: allNewsData[sender.tag].is_bookmarked ?? 0)
        }
     
    }
    
    //MARK:- SHARE TAPPED
    @objc func shareTapped(_ sender : UIButton) {
            let text = "I found this news article on Wats'on News application , Tap here to read it out : \n \(allNewsData[sender.tag].link ?? "Link not found")"
             // set up activity view controller
             let textToShare = [ text ]
             let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
             activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
             // present the view controller
             self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    
    //MARK:- READ FULL TAPPED
    @objc func readFullTapped(_ sender: UIButton) {
        if let url = URL(string: allNewsData[sender.tag].link ?? "www.google.com") {
            UIApplication.shared.open(url)
        }
    }
    
    //MARK:- SCROLL VIEW DID SCROL
    //WILL BE CALLED WHENEVER SCROLL VIEW END SCROLLING
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //CHECKING IF TABLE VIEW IS THE SCROLL VIEW
        if scrollView == self.tableView {
            var visibleIndex = 0
            DispatchQueue.main.async {
                //GOING FOR FORLOOP IN VISIBLE CELLS OF TABLE VIEW
                for cells in self.tableView.visibleCells {
                    let indexpath = self.tableView.indexPath(for: cells)
                    if indexpath?.row != 0 {
                        visibleIndex = indexpath?.row ?? 0
                    }
                }
                //ABOVE CODE WILL RETURN THE VALUE OF VISIBLE INDEX
                //CHECKING IF VISIBLE INDEX IS FIRST CELL BEING SHOWN, IF IT IS FIRST CELL, SHOW REFRESH , OTHERWISE GO TO TOP
                if visibleIndex == 0 {
                    self.refreshLabel.text = "Refresh"
                    self.refreshImage.image = R.image.refresh()
                }else {
                    self.refreshLabel.text = "Top"
                    self.refreshImage.image = R.image.top()
                }
            }
        }
    }
    

   // MARK:- SCROLL VIEW DID END DECELERATING
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
          let totalPages = allNewsMeta?.totalpage ?? 1
          let currentPages = Int(allNewsMeta?.curernt_page ?? "1")!
          let remainingPagesCount = totalPages - currentPages
          let maximumOffset = scrollView.contentSize.height - (scrollView.frame.size.height)
             if scrollView == self.tableView{
                if (scrollView.contentOffset.y == /*0*/ maximumOffset ) {
                  if remainingPagesCount < 1{
                    PrintToConsole("No More News")
                  }else{
                    pageNo = pageNo + 1
                    self.apiCallForGetNewsOFParticularSources()
                  }
              }
          }
      }

}
