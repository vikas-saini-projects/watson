//
//  TableViewCellForFeed.swift
//  Watson
//
//  Created by Techwin on 17/02/21.
//

import UIKit
import SDWebImage
class TableViewCellForFeed: UITableViewCell {

    
    //MARK:- OUTLETS
    
    //views
    @IBOutlet weak var viewBehindBottomView: UIView!
    @IBOutlet weak var BottomView: UIView!
    @IBOutlet weak var optionView: UIView!
    @IBOutlet weak var viewBehindImageView: UIView!
    //imageviews
    @IBOutlet weak var feedImageView: UIImageView!
    @IBOutlet weak var likeImageView: UIImageView!
    @IBOutlet weak var bookmarkImage: UIImageView!
    @IBOutlet weak var editorChoiceImageView: UIImageView!
    @IBOutlet weak var shareImageView: UIImageView!
    //Buttons
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var bookMarkButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var editorButton: UIButton!
    @IBOutlet weak var readFullButton: UIButton!
    //label
    @IBOutlet weak var likesCountLabel: UILabel!
    @IBOutlet weak var CategoryNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    
    //MARK:- AWAKE FROM NIB
    override func awakeFromNib() {
        super.awakeFromNib()
        setupShadowAndCorners()
        shareImageView.tintColor = UIColor.darkGray
    }
    
    //MARK:- SET UP SHADOW AND CORNERS
    func setupShadowAndCorners() {
        DispatchQueue.main.async {
            self.readFullButton.roundButtonCorner(radius: 08)
            self.viewBehindImageView.giveShadowAndRoundCorners(shadowOffset: CGSize.zero, shadowRadius: 12, opacity: 0.8, shadowColor: R.color.mainColor() ?? MAIN_COLOR, cornerRadius: 0)
            self.feedImageView.roundCorners(corners: [.topRight , .bottomLeft ], radius: 30)
            self.feedImageView.clipsToBounds = true
            self.BottomView.roundCorners(corners: [.topRight , .bottomLeft], radius: 30)
            self.viewBehindBottomView.giveShadowAndRoundCorners(shadowOffset: CGSize.zero, shadowRadius: 12, opacity: 0.8, shadowColor: R.color.mainColor() ?? MAIN_COLOR, cornerRadius: 0)
            self.optionView.giveShadowAndRoundCorners(shadowOffset: CGSize.zero, shadowRadius: 05, opacity: 0.8, shadowColor: R.color.mainColor() ?? MAIN_COLOR, cornerRadius: self.optionView.layer.bounds.width / 2)
        }
    }
    
    
    //MARK:- POPULATE DATA
    func populateData(_ news1 : AllNewsModelDataClass?) {
        if let news = news1 {
            //populate Data here
            selectionStyle = .none
            feedImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
            feedImageView.sd_setImage(with: URL(string: news.media ?? "https://www.momscleanairforce.org/wp-content/uploads/2017/01/fake_news.jpg"), placeholderImage: nil, options: [.highPriority], context: nil)
            descriptionLabel.text = news.description ?? ""
            CategoryNameLabel.text = news.category_name ?? ""
            if news.liked == 0 {
                likesCountLabel.text = "No likes"
            }else if news.liked == 1 {
                likesCountLabel.text = "1 like"
            }else {
                likesCountLabel.text = "\(news.liked ?? 0) likes"
            }
            if news.is_liked != 0 {
                self.likeImageView.tintColor = R.color.mainColor()
            }else {
                self.likeImageView.tintColor = UIColor.darkGray
            }
            if news.editor_choice != "0" {
                self.editorChoiceImageView.tintColor = R.color.mainColor()
            }else {
                self.editorChoiceImageView.tintColor = UIColor.darkGray
            }
            if news.is_bookmarked != 0 {
                self.bookmarkImage.tintColor = R.color.mainColor()
            }else {
                self.bookmarkImage.tintColor = UIColor.darkGray
            }
        }
    }
}
