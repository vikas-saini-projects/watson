//
//  BookmarkModel.swift
//  Watson
//
//  Created by Techwin on 25/02/21.
//

import Foundation

// MARK: - BookmarkModel
struct BookmarkModel: Codable {
    let status: Int?
    let message, method: String?
}
