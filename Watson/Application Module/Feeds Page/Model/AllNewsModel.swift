//
//  AllNewsModel.swift
//  Watson
//
//  Created by Techwin on 24/02/21.
//

import Foundation


// MARK: - AllNewsModel
struct AllNewsModel: Codable {
    var status: Int?
    var message: String?
    var meta_data: AllNewsModelMetaDataClass?
    var data: [AllNewsModelDataClass]?
    var method: String?
}

// MARK: - AllNewsModelDataClass
struct AllNewsModelDataClass: Codable , Equatable{
    var news_id, category_id, category_name, resource_id: String?
    var news_type: String?
    var title, description: String?
    var link: String?
    var category, pubDate, guid: String?
    var media: String?
    var editor_choice, isactive: String?
    var created_at: String?
    var updated_at: String?
    var liked, unliked, is_liked, is_unliked: Int?
    var is_bookmarked:Int?
   
}

// MARK: - AllNewsModelMetaDataClass
struct AllNewsModelMetaDataClass: Codable {
    var totalpage: Int?
    var curernt_page: String?
}


//MARK:- MAKING MODEL EQUATABLE
func ==(lhs: AllNewsModelDataClass, rhs: AllNewsModelDataClass) -> Bool {
    return lhs.news_id == rhs.news_id && lhs.category_id == rhs.category_id
}
