// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let likeUnlikeModel = try? newJSONDecoder().decode(LikeUnlikeModel.self, from: jsonData)

import Foundation

// MARK: - LikeUnlikeModel
struct LikeUnlikeModel: Codable {
    let status: Int?
    let message: String?
    let data: LikeUnlikeModelDataClass?
    let method: String?
}

// MARK: - LikeUnlikeModelDataClass
struct LikeUnlikeModelDataClass: Codable {
    let id, news_id, user_id, status: String?
    let is_active, created_at: String?
    let updated_at: String?

}

