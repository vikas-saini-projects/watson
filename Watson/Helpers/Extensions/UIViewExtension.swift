//
//  UIViewExtension.swift
//  BaseProject
//
//  Created by Techwin on 21/12/20.
//

import Foundation
import UIKit




 extension UIView {
    
    // MARK:- Custom Shadow to View
    //CONTRIBUTED BY INDERJEET SIR

    func addshadow(top: Bool,
                  left: Bool,
                  bottom: Bool,
                  right: Bool) {
       let path = UIBezierPath()
       var x: CGFloat = 0
       var y: CGFloat = 0
       var viewWidth = self.frame.width
       var viewHeight = self.frame.height
       
       // here x, y, viewWidth, and viewHeight can be changed in
       // order to play around with the shadow paths.
       if (!top) {
           y+=(0)
       }
       if (!bottom) {
           viewHeight-=(0)
       }
       if (!left) {
           x+=(0+1)
       }
       if (!right) {
           viewWidth-=(0)
       }
       // selecting top most point
       path.move(to: CGPoint(x: x, y: y))
       // Move to the Bottom Left Corner, this will cover left edges
       /*
        |☐
        */
       path.addLine(to: CGPoint(x: x, y: viewHeight))
       // Move to the Bottom Right Corner, this will cover bottom edge
       /*
        ☐
        -
        */
       path.addLine(to: CGPoint(x: viewWidth, y: viewHeight))
       // Move to the Top Right Corner, this will cover right edge
       /*
        ☐|
        */
       path.addLine(to: CGPoint(x: viewWidth, y: y))
       // Move back to the initial point, this will cover the top edge
       /*
        _
        ☐
        */
       path.close()
       self.layer.shadowPath = path.cgPath
   }
    
    
    
    //MARK:- ROUND VIEW CORNERS
    //CONTRIBUTED BY INDERJEET SINGH
    func roundViewCorner(radius : CGFloat){
        DispatchQueue.main.async {
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
        }
    }
    
    
    
    //MARK:- ROUND CORNERS AND GIVING SHADOW

    func giveShadowAndRoundCorners(shadowOffset: CGSize , shadowRadius : Int , opacity : Float , shadowColor : UIColor , cornerRadius :
    CGFloat){
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = true
        DispatchQueue.main.async {
            self.layer.shadowPath =  UIBezierPath(roundedRect: self.bounds,cornerRadius: self.layer.cornerRadius).cgPath
        }
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = CGFloat(shadowRadius)
        self.layer.shadowOffset = shadowOffset
        self.layer.masksToBounds = false
    }
    
    
    //MARK:- MARK PARTICULAR CORNERS OF A VIEW
      func roundCorners(corners: UIRectCorner, radius: CGFloat) {
            let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            layer.mask = mask
        }
}


