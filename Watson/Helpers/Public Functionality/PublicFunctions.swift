//
//  PublicFunctions.swift
//  DummyProject
//
//  Created by Vikas saini on 16/01/21.
//

import Foundation

//MARK:- PRINT TO CONSOLE

//CONTRIBUTED BY VIKAS SAINI DATED 16 JAN 2021

//FUNCTION USE PRINT STATEMENT TO PRINT ONLY IN DEBUG MODE AND PREVENT TO  PRINT TO CONSOLE IF IS NOT DEBUGGING
//PRINT STATEMENT IN RELEASE MODE CAN SLOW DOWN THE APPLICATION

public func PrintToConsole(_ message: String) {
    #if DEBUG
    print(message)
    #endif
}

//USAGE :  PrintToConsole("SomeMessage")
