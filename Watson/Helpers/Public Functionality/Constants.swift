//
//  Constants.swift
//  Watson
//
//  Created by Techwin on 16/02/21.
//

import Foundation
import UIKit

//MARK:- GLOBAL CONSTANTS
let MAIN_COLOR = UIColor(displayP3Red: 112/255, green: 36/255, blue: 209/255, alpha: 1.0)
//let PINKISH_COLOR = UIColor(displayP3Red: 245/255, green: 105/255, blue: 147/255, alpha: 1.0)
let DEVICE_TYPE = 1
let DEVICE_TOKEN = "DEVICE_TOKEN_DUMMY"

//MARK:- USER DEFAULTS CONSTANTS
let UD_USERID = "UD_USERID"
let UD_IMAGEURL = "UD_IMAGEURL"
let UD_NAME = "UD_NAME"
let UD_EMAIL = "UD_EMAIL"

//MARK:- BASE URL
let BASE_URL = "http://techwinlabs.in/watson/"

//MARK:- END POINTS
let LOGIN_API = "auth/social_login"//POST
let SOURCES_API = "auth/Resources" //POST
let NEWS_API = "auth/news"//GET
let LIKE_UNLIKE_API = "auth/LikeUnlike"//POST
let BOOKMARK_API = "auth/DoBookmark"//POST
let GET_BOOKMARKED_API = "auth/GetBookMarks"//POST
let GET_EDITORCHOICE_API = "auth/GetEditorChoice"//POST
