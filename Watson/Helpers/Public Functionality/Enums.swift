//
//  Enums.swift
//  DummyProject
//
//  Created by Vikas saini on 26/01/21.
//

import Foundation

//MARK:- ENUM FOR METHOD TYPE
enum MethodType : String {
    case Post = "POST"
    case Get = "GET"
    case Put = "PUT"
    case Patch = "PATCH"
    case Delete = "DELETE"
}

//MARK:- ENUM FOR LOGIN TYPE

enum LoginType : Int {
    case Facebook = 1
    case Google = 2
    case apple = 3
}
