//
//  Protocols.swift
//  Watson
//
//  Created by Techwin on 17/02/21.
//

import Foundation


//MARK:- HIDE SIDE MENU PROTOCOL

//TO BE USED WHEN USER CLICK ON THE TRANSPARENT VIEW OF PROFILE VIEW CONTROLLER , THIS PROTOCOL WILL SEND A MESSAGE TO FEED CONTROLLER TO PUSH THE PROFILE MENU OUT OF SIGHT
protocol HideProfileMenuProtocol {
    func HideSideMenu()
}
