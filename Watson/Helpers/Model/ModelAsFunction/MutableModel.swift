
//CONTRIBUTED BY SHIV KUMAR ON FEB 15 , 2021

import Foundation

struct Receipt{
    var ID:Int = 0
    var productName:String = ""
    var productPrice:Double = 0.0
    var isPaid:Bool = false

    mutating func makePurchase(name: String, price: Double){
        productName = name
        productPrice = price
        isPaid = true
    }
}

/*
// MARK: USAGE

var receipt = Receipt()
receipt.ID = 99

receipt.makePurchase(name: "Beer", price: "4.99")
print("Bought a \(receipt.productName) for $ \(receipt.productPrice)")


//MARK: DESCRIPTION

The makePurchase(name:price:) function can now be used to change the properties of the Receipt object, to “simulate” making a purchase.

Why is the makePurchase(...) function marked with the mutating keyword? That’s because the function changes the properties of the current instance. If you want to create a function that can mutate (or change) the properties of a struct, you have to explicitly declare the function with mutating. It’s a safeguard to prevent you from accidentally changing the object’s values.
*/
